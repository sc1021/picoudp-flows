# PicoUDP-Flows
This Repo provides Example Node-Red-Flows for PicoUDP: https://gitlab.com/sc1021/picoudp


## Getting started
1. Set Up PicoUDP
2. Visit http://localhost:1880/
3. Select "Clone Repository"
4. Enter something as Username and Mailadress
5. As "Git Repository URL" enter:  https://gitlab.com/sc1021/picoudp-flows.git
6. Enter "Credentials Encryption Key" (Write me a Mail: marius.henkel@kaiserslautern.de)
